import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../search.service';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {

  public filters;

  constructor(public service:SearchService) {
    this.service.filters.subscribe(filters => this.filters = filters);
  }

  ngOnInit() {
  }

  updateTerm(term:string) {
    this.service.updateFilters({...this.filters, term: term});
  }

}
