import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../search.service';

@Component({
  selector: 'search-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  public total;

  constructor(public service:SearchService) {
    this.service.total.subscribe(total => this.total = total);
  }

  ngOnInit() {
  }

}
