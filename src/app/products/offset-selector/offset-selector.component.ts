import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../search.service';

@Component({
  selector: 'offset-selector',
  templateUrl: './offset-selector.component.html',
  styleUrls: ['./offset-selector.component.scss']
})
export class OffsetSelectorComponent implements OnInit {

  public filters;

  constructor(public service:SearchService) {
    this.service.filters.subscribe(filters => this.filters = filters);
  }

  ngOnInit() {

  }

  updateTake(val) {
    this.service.updateFilters({...this.filters, take: parseInt(val)});
  }

}
