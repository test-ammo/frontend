import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchTitleComponent } from './search-title/search-title.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SummaryComponent } from './products/summary/summary.component';
import { SearchService } from './search.service';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SearchInputComponent } from './header/search-input/search-input.component';
import { FormsModule } from '@angular/forms';
import { OffsetSelectorComponent } from './products/offset-selector/offset-selector.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchTitleComponent,
    ProductsComponent,
    ProductComponent,
    PaginationComponent,
    SummaryComponent,
    SearchInputComponent,
    OffsetSelectorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [SearchService, Subject],
  bootstrap: [AppComponent]
})
export class AppModule { }
