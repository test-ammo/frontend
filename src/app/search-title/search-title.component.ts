import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'search-title',
  templateUrl: './search-title.component.html',
  styleUrls: ['./search-title.component.scss']
})
export class SearchTitleComponent implements OnInit {

  public filters;

  constructor(public service:SearchService) {
    this.service.filters.subscribe(filters => this.filters = filters);
  }

  ngOnInit() {
  }

}
